/*
 * file:        homework.c
 * description: skeleton file for CS 5600 homework 3
 *
 * CS 5600, Computer Systems, Northeastern CCIS
 * Peter Desnoyers, updated April 2012
 * $Id: homework.c 452 2011-11-28 22:25:31Z pjd $
 */

#define FUSE_USE_VERSION 27

#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <fuse.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <assert.h>
#include <time.h>

#include "cs5600fs.h"
#include "blkdev.h"

typedef int BOOL;
#define true 1
#define false 0 

#define FAT_ENTRY_COUNT(blocks) (blocks * 256)
#define NEGATIVE_RESULT(result) ((result > 0) ? (-1 * result) : result)
#define INT_DIV_CEILING(NUM, DIV) ((NUM/DIV) + ((NUM % DIV == 0) ? 0 : 1))

/* 
 * disk access - the global variable 'disk' points to a blkdev
 * structure which has been initialized to access the image file.
 *
 * NOTE - blkdev access is in terms of 512-byte SECTORS, while the
 * file system uses 1024-byte BLOCKS. Remember to multiply everything
 * by 2.
 */

extern struct blkdev *disk;

/*
 * superblock struct
 */
struct cs5600fs_super superblock;

/*
 * file allocation table
 */
struct cs5600fs_entry *fat;

/*
 * function prototypes
 */
 /* blkdev */
int blkdev_read(int first_blk, int num_blks, char *buf);
int blkdev_write(int first_blk, int num_blks, char *buf);

/* directory structure */
static void path_split(const char *path, const char *delim, char ***parts, int *count);
static int dirent_get_by_path(const char *path, struct cs5600fs_dirent *dirent);
static int dirent_get_by_parts(char **parts, int count, struct cs5600fs_dirent *dirent);
static int dirent_set_by_path(const char *path, struct cs5600fs_dirent *dirent);
static int dirent_set_by_parts(char **parts, int count, struct cs5600fs_dirent *dirent);
static int dirent_by_path(const char *path, struct cs5600fs_dirent *dirent, BOOL doRead);
static int dirent_by_parts(char **parts, int count, struct cs5600fs_dirent *dirent, BOOL doRead);
static int dirent_get_by_name(struct cs5600fs_dirent *dir, const char *name, struct cs5600fs_dirent *entry, int *direntIndex);
static int dirent_modify_by_path(const char *path, int valid, int isDir, int start, int length);
static int dirent_create(const char *path, mode_t mode, int isDir);

/* fat */
static int fat_allocate_blocks(const int count);
static int fat_free_blocks(const int firstBlock);
int fat_flush();

/*
 * min function
 */
int min(int a, int b)
{
    return (a < b) ? a : b;
}

/*
 * max function
 */
int max(int a, int b)
{
    return (a > b) ? a : b;
}

/*
 * fat_allocate_blocks
 *
 * Allocates "count" number of free blocks in the FAT and returns the
 * index of the first block. If there are not enough free blocks, -1
 * will be returned.
 */
static int fat_allocate_blocks(const int count)
{
    /* return an error if we are not trying to allocate at least one block */
    if (count < 1)
    {
        return -1;
    }

    /* initialize local variables */
    int i = 0, allocated = 0;
    int *blocks = (int *)malloc(sizeof(int) * count);

    /* iterate over the FAT finding free blocks*/
    for (; i < FAT_ENTRY_COUNT(superblock.fat_len) && allocated < count; i++)
    {
        if (!fat[i].inUse)
        {
            blocks[allocated++] = i;
        }
    }

    /* return an error if we have not found enough free blocks */
    if (allocated != count)
    {
        return -1;
    }

    /* make changes to FAT */
    for (i = 0; i < count - 1; i++)
    {
        fat[blocks[i]].inUse = 1;
        fat[blocks[i]].eof = 0;
        fat[blocks[i]].next = blocks[i + 1];
    }

    fat[blocks[count - 1]].inUse = 1;
    fat[blocks[count - 1]].eof = 1;

    /* return the index of the first block */
    int firstBlock = blocks[0];
    free(blocks);
    return firstBlock;
}

/*
 * fat_extend_blocks
 *
 * extends the list of blocks by count number of new blocks
 */
static void fat_extend_blocks(struct cs5600fs_entry *block, int count)
{
    struct cs5600fs_entry *currentBlock = block;
    assert(currentBlock->inUse);

    //traverse block list to eof block
    while (currentBlock->eof == 0)
    {
        currentBlock = &fat[currentBlock->next];
        assert(currentBlock->inUse);
    }

    //allocate new string of blocks and concatenate
    int firstOfNewBlocks = fat_allocate_blocks(count);
    currentBlock->eof = 0;
    currentBlock->next = firstOfNewBlocks;
}

/*
 * fat_free_blocks
 *
 * Frees the set of blocks starting at "firstBlock", ending on the first block
 * that has EOF set to 1. If the given block index is not valid, returns -1.
 * Returns 0 upon success.
 */
static int fat_free_blocks(const int firstBlock)
{
    /* return an error if given an invalid block index */
    if (firstBlock < 0 || firstBlock >= FAT_ENTRY_COUNT(superblock.fat_len) || fat[firstBlock].inUse == 0)
    {
        return -1;
    }

    int i = firstBlock;

    //free all non-EOF entries
    while (fat[i].eof == 0)
    {
        fat[i].inUse = 0;
        i = fat[i].next;
    }

    //free EOF entry
    fat[i].inUse = 0;

    /* return success */
    return 0;
}

/*
 * fat_flush
 *
 * Writes the in-memory file allocation table (fat) to disk
 */
int fat_flush()
{
    int result = blkdev_write(1, superblock.fat_len, (char *)fat);
	return NEGATIVE_RESULT(result);
}


/*
 * blkdev_read
 *
 * Wrapper around disk->ops->read() that lets us think in terms of 
 * 1024 byte block sizes
 */
int blkdev_read(int first_blk, int num_blks, char *buf)
{
    return disk->ops->read(disk, 2*first_blk, 2*num_blks, buf);
}

/*
 * blkdev_write
 *
 * Wrapper around disk->ops->write() that lets us think in terms of 
 * 1024 byte block sizes
 */
int blkdev_write(int first_blk, int num_blks, char *buf)
{
    return disk->ops->write(disk, 2*first_blk, 2*num_blks, buf);
}

/* init - this is called once by the FUSE framework at startup.
 * This might be a good place to read in the super-block and set up
 * any global variables you need. You don't need to worry about the
 * argument or the return value.
 */
void* hw3_init(struct fuse_conn_info *conn)
{
    blkdev_read(0, 1, (char *)&superblock);
    fat = malloc(1024 * superblock.fat_len);
    blkdev_read(1, superblock.fat_len, (char *)fat);
    
    return NULL;
}

/* Note on path translation errors:
 * In addition to the method-specific errors listed below, almost
 * every method can return one of the following errors if it fails to
 * locate a file or directory corresponding to a specified path.
 *
 * ENOENT - a component of the path is not present.
 * ENOTDIR - an intermediate component of the path (e.g. 'b' in
 *           /a/b/c) is not a directory
 */
static void path_split(const char *path, const char *delim, char ***parts, int *count)
{
    char *pathdup = strdup(path);
    
    (*count) = 0;
    (*parts) = NULL;
    char *part = NULL;

    while ((part = strsep(&pathdup, delim)))
    {
        if (strlen(part))
        {
            (*parts) = (char **)realloc((void *)(*parts), sizeof(char *) * ((*count) + 1));
            (*parts)[(*count)++] = part;
        }
    }
}

/*
 * dirent_get_by_path
 *
 * Gets a dirent given the "path" from root ("/") and returns 0 for success or
 * an error code.
 */
static int dirent_get_by_path(const char *path, struct cs5600fs_dirent *dirent)
{
    return dirent_by_path(path, dirent, true);
}

/*
 * dirent_set_by_path
 *
 * Sets a dirent given the "path" from root ("/") and returns 0 for success or
 * an error code.
 */
static int dirent_set_by_path(const char *path, struct cs5600fs_dirent *dirent)
{
    return dirent_by_path(path, dirent, false);
}

static int dirent_by_path(const char *path, struct cs5600fs_dirent *dirent, BOOL doRead)
{
    int count = 0;
    char **parts = NULL;

    /* return error if the path does not start with root directory */
    if (path[0] != '/')
        return -ENOENT;

    /* split path into parts */
    path_split(path, "/", &parts, &count);

    /* if we are only asking for "/", just return the root dirent */
    if (count == 0)
    {
        memcpy((void *)dirent, (const void *)&(superblock.root_dirent), sizeof(struct cs5600fs_dirent));
        return 0;
    }

    /* get dirent based on path components, return success or error code */
    int result = dirent_by_parts(parts, count, dirent, doRead);
    free(parts);
    return result;
}

/*
 * dirent_get_by_parts
 *
 * Given an array of path component strings and the number of strings, fill
 * in the dirent with the appropriate entry.
 */
static int dirent_get_by_parts(char **parts, int count, struct cs5600fs_dirent *dirent)
{
    return dirent_by_parts(parts, count, dirent, true);
}

/*
 * dirent_set_by_parts
 *
 * Given an array of path component strings and the number of strings, 
 * write dirent to the blkdev
 */
static int dirent_set_by_parts(char **parts, int count, struct cs5600fs_dirent *dirent)
{
    return dirent_by_parts(parts, count, dirent, false);
}

static int dirent_by_parts(char **parts, int count, struct cs5600fs_dirent *dirent, BOOL doRead)
{
    int result = 0;

    /* copy the root dirent if there are no path components */
    if (count == 0 || parts == NULL)
    {
        if (doRead)
        {
            memcpy((void *)dirent, (const void *)&(superblock.root_dirent), sizeof(struct cs5600fs_dirent));
        }
        else
        {
            superblock.root_dirent = *dirent;
            if ((result = blkdev_write(0, 1, (char *)&superblock)) != 0)
                return NEGATIVE_RESULT(result);
        }

        return 0;
    }

    int i = 0;

    struct cs5600fs_dirent *current = &(superblock.root_dirent);
    struct cs5600fs_dirent next = {0};
    int direntIndex = 0;
    for (; i < (count - 1); i++)
    {
        int result = dirent_get_by_name(current, parts[i], &next, &direntIndex);

        if (result != 0)
            return result;

        if (!next.isDir)
            return -ENOTDIR;

        current = &next;
    }

    if (doRead)
    {
        return dirent_get_by_name(current, parts[count - 1], dirent, &direntIndex);
    }
    else
    {
		int result = 0;
		
        dirent_get_by_name(current, parts[count - 1], NULL, &direntIndex);
        struct cs5600fs_dirent directory[16];
        
		if ((result = blkdev_read(current->start, 1, (char *)&directory)) != 0)
			return NEGATIVE_RESULT(result);
		
        directory[direntIndex] = *dirent;
		
        if ((result = blkdev_write(current->start, 1, (char *)&directory)) != 0)
			return NEGATIVE_RESULT(result);
    }
	
	return 0;
}

/*
 * dirent_get_by_name
 *
 * Given a starting directory and entry name, retrieve the appropriate entry
 * and return a success or failure code.
 */
static int dirent_get_by_name(struct cs5600fs_dirent *dir, const char *name, struct cs5600fs_dirent *entry, int *direntIndex)
{
    /* there are 16 possible entries in a directory (one block) */
    int i;
    struct cs5600fs_dirent entries[16];
    int result = blkdev_read(dir->start, 1, (char *) entries);

    /* return if an error occurs */
    if (result != 0)
        return NEGATIVE_RESULT(result);

    /* otherwise look for the name in the directory */
    for (i = 0; i < 16; i++)
    {
        if (entries[i].valid)
        {
            if(!strcmp(name, entries[i].name))
            {
                /* this part of the path exists, fill in the dirent, return success */
                if (direntIndex != NULL)
                    *direntIndex = i;
                if (entry != NULL)
                    memcpy((void *)entry, (const void*)&(entries[i]), sizeof(struct cs5600fs_dirent));
                return 0;
            }
        }
    }

    /* this part of the path does not exist */
    return -ENOENT;
}

/*
 * dirent_modify_by_path
 *
 * Helper function used by hw3_unlink, hw3_truncate, and hw3_rmdir
 */
static int dirent_modify_by_path(const char *path, int valid, int isDir, int start, int length)
{
    struct cs5600fs_dirent dirent;

    /* get the dirent to modify from the path */
    int result = dirent_get_by_path(path, &dirent);
    if (result != 0)
        return result;

    /* if we are trying to modify a file and the path is a directory */
    if (dirent.isDir && !isDir)
        return -EISDIR; /* then return EISDIR */
    /* if we are trying to modify a directory and the path is a file */
    else if (!dirent.isDir && isDir)
        return -ENOTDIR; /* then return ENOTDIR */

    /* if entry is invalidated or file is truncated, free blocks */
    if ((!isDir && length == 0) || !valid)
    {
        if (isDir)
        {
            /* check to see if the directory is empty */
            struct cs5600fs_dirent entries[16] = {{0}};
            if ((result = blkdev_read(dirent.start, 1, (char *)entries)) != 0)
				return NEGATIVE_RESULT(result);

            int i = 0;
            for (; i < 16; i++)
            {
                if (entries[i].valid)
                    return -ENOTEMPTY; /* oops, directory not empty */
            }
        }

        result = fat_free_blocks(dirent.start);
        if (result != 0)
            return result;
        result = fat_flush();
		if (result != 0)
			return result;
    }

    /* make changes to dirent in memory */
    dirent.valid = valid;
    dirent.isDir = isDir;
    dirent.start = start;
    dirent.length = length;

    /* write changes to disk */
    return dirent_set_by_path(path, &dirent);
}

/*
 * dirent_create
 *
 * Creates a new directory entry with the given path, mode (mode for
 * directories, mode & 01777 for files), and type (directory or file)
 */
static int dirent_create(const char *path, mode_t mode, int isDir)
{
    struct cs5600fs_dirent dirent = {0};
    char **parts = NULL;
    int count = 0, i = 0, result = 0;

    /* split the path */
    path_split(path, "/", &parts, &count);

    /* check to see if the directory entry already exists */
    result = dirent_get_by_parts(parts, count, &dirent);

    /* file already exists or an error other than -ENOENT occured */
    if (result != -ENOENT)
    {
        free(parts);
        if (result == 0)
            return -EEXIST;
        return result;
    }

    /* get the parent directory */
    result = dirent_get_by_parts(parts, count - 1, &dirent);

    if (result != 0)
    {
        free(parts);
        return result;
    }

    if (strlen(parts[count - 1]) > 43)
    {
        free(parts);
        return -EINVAL;
    }

    /* read the parent directory block */
    struct cs5600fs_dirent entries[16] = {{0}};
	result = blkdev_read(dirent.start, 1, (char *)&entries);
	if (result != 0)
    {
        free(parts);
		return NEGATIVE_RESULT(result);
    }

    /* iterate over the parent directory and find an invalid dirent to use */
    for (; i < 16; i++)
    {
        if (!entries[i].valid)
        {
            /* create entry */
            int start = (isDir)?fat_allocate_blocks(1):-1;
            
            if (!isDir || start != -1)
            {
                entries[i].mtime = time(NULL);
                entries[i].uid = getuid();
                entries[i].gid = getgid();
                entries[i].valid = 1;
                entries[i].isDir = isDir;
                entries[i].mode = (isDir)?mode:(mode & 01777);
                strcpy(entries[i].name, parts[count - 1]);
                entries[i].start = start;
                entries[i].length = 0;

                free(parts);

                /* write changes to disk and return */
                if ((result = blkdev_write(dirent.start, 1, (char *)&entries)) != 0)
					return NEGATIVE_RESULT(result);
				
                return 0;
            }
        }
    }

    /* there was no space, return ENOSPC */
    free(parts);
    return -ENOSPC;
}

/*
 * direntToStat
 *
 * Helper function that converts directory entries to stat structures
 */
void direntToStat(struct cs5600fs_dirent *dirent, struct stat *sb)
{
    assert(dirent->valid);

    memset(sb, 0, sizeof(struct stat));
    
    sb->st_dev = 0;
    sb->st_ino = 0;
    sb->st_mode = dirent->mode | (dirent->isDir ? S_IFDIR : S_IFREG);
    sb->st_nlink = 1;
    sb->st_uid = dirent->uid;
    sb->st_gid = dirent->gid;
    sb->st_rdev = 0;
    sb->st_size = dirent->length;
    sb->st_blksize = 0; 
    sb->st_blocks = (dirent->isDir) ? 0 : (dirent->length == 0 ? 0 : INT_DIV_CEILING(dirent->length, 1024));
    sb->st_atime = dirent->mtime;
    sb->st_mtime = dirent->mtime;
    sb->st_ctime = dirent->mtime;
}

/* getattr - get file or directory attributes. For a description of
 *  the fields in 'struct stat', see 'man lstat'.
 *
 * Note - fields not provided in CS5600fs are:
 *    st_nlink - always set to 1
 *    st_atime, st_ctime - set to same value as st_mtime
 *
 * errors - path translation, ENOENT
 */
static int hw3_getattr(const char *path, struct stat *sb)
{
    //read attributes from disk
    struct cs5600fs_dirent dirent = {0};
    int result = dirent_get_by_path(path, &dirent);
    if (result != 0)
    {
         return result;
    }

    //convert to struct stat
    direntToStat(&dirent, sb);

    return 0;
}

/* readdir - get directory contents.
 *
 * for each entry in the directory, invoke the 'filler' function,
 * which is passed as a function pointer, as follows:
 *     filler(buf, <name>, <statbuf>, 0)
 * where <statbuf> is a struct stat, just like in getattr.
 *
 * Errors - path resolution, ENOTDIR, ENOENT
 */
static int hw3_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
		       off_t offset, struct fuse_file_info *fi)
{
    //get dirent for target directory
    struct cs5600fs_dirent dirent = {0};
    int result = dirent_get_by_path(path, &dirent);
    if (result != 0)
        return result;
    
    assert(dirent.valid);

    if (!dirent.isDir)
        return ENOTDIR;

    //read directory block (get block # from dirent)
    struct cs5600fs_dirent directory[16];
    memset(directory, 0, sizeof(directory));
    result = blkdev_read(dirent.start, 1, (char *)&directory);
	if (result != 0)
		return NEGATIVE_RESULT(result);

    //iterate over all the files in a directory and invoke the 'filler' function for each.
    int i = 0;
    for (; i < 16; ++i) 
    {
        struct cs5600fs_dirent *direntCursor = &directory[i];
        if (!direntCursor->valid)
            continue;

        //convert to struct stat
        struct stat sb = {0};
        direntToStat(direntCursor, &sb);

        //callback
        filler(buf, direntCursor->name, &sb, 0);
    }

    return 0;
}

/* create - create a new file with permissions (mode & 01777)
 *
 * Errors - path resolution, EEXIST
 *
 * If a file or directory of this name already exists, return -EEXIST.
 */
static int hw3_create(const char *path, mode_t mode,
			 struct fuse_file_info *fi)
{
    return dirent_create(path, mode, 0);
}

/* mkdir - create a directory with the given mode.
 * Errors - path resolution, EEXIST
 * Conditions for EEXIST are the same as for create.
 */ 
static int hw3_mkdir(const char *path, mode_t mode)
{
    return dirent_create(path, mode, 1);
}

/* unlink - delete a file
 *  Errors - path resolution, ENOENT, EISDIR
 */
static int hw3_unlink(const char *path)
{
    return dirent_modify_by_path(path, 0, 0, -1, 0);
}

/* rmdir - remove a directory
 *  Errors - path resolution, ENOENT, ENOTDIR, ENOTEMPTY
 */
static int hw3_rmdir(const char *path)
{
    return dirent_modify_by_path(path, 0, 1, -1, 0);
}

/* rename - rename a file or directory
 * Errors - path resolution, ENOENT, EINVAL, EEXIST
 *
 * ENOENT - source does not exist
 * EEXIST - destination already exists
 * EINVAL - source and destination are not in the same directory
 *
 * Note that this is a simplified version of the UNIX rename
 * functionality - see 'man 2 rename' for full semantics. In
 * particular, the full version can move across directories, replace a
 * destination file, and replace an empty directory with a full one.
 */
static int hw3_rename(const char *src_path, const char *dst_path)
{
    struct cs5600fs_dirent src, dst;
    int srcCount = 0, dstCount = 0, i = 0, result = 0;
    char **srcParts = NULL, **dstParts = NULL;

    path_split(src_path, "/", &srcParts, &srcCount);
    path_split(dst_path, "/", &dstParts, &dstCount);

    /* check validity of src and dst path */
    if (srcCount != dstCount)
    {
        result = -EINVAL;
        goto end;
    }
    
    /* check filename len */
    if (strlen(dstParts[(dstCount - 1)]) > 43)
    {
        result = -EINVAL;
        goto end;
    }
    
    for (; i < (srcCount - 1); i++)
    {
        if (strcmp(srcParts[i], dstParts[i]))
        {
            result = -EINVAL;
            goto end;
        }
    }

    /* check validity of source */
    result = dirent_get_by_parts(srcParts, srcCount, &src);
    if (result != 0)
        goto end;

    /* check validity of destination */
    result = dirent_get_by_parts(dstParts, dstCount, &dst);
    if (result != -ENOENT)
    {
        if (result == 0)
            result = -EEXIST;
        goto end;
    }

    /* set the name of the dirent to the destination name */
    strcpy(src.name, dstParts[(dstCount - 1)]);
    result = dirent_set_by_parts(srcParts, srcCount, &src);

    /* end label, frees everything and returns the result */
    end:
    free(srcParts);
    free(dstParts);
    return result;
}

/* chmod - change file permissions
 * utime - change access and modification times
 *         (for definition of 'struct utimebuf', see 'man utime')
 *
 * Errors - path resolution, ENOENT.
 */
static int hw3_chmod(const char *path, mode_t mode)
{
    struct cs5600fs_dirent dirent = {0};
    int result = dirent_get_by_path(path, &dirent);
    if (result != 0)
        return result;

    dirent.mode = mode;
    return dirent_set_by_path(path, &dirent);
}

int hw3_utime(const char *path, struct utimbuf *ut)
{
    struct cs5600fs_dirent dirent = {0};
    int result = dirent_get_by_path(path, &dirent);
    if (result != 0)
        return result;

    dirent.mtime = ut->modtime;
    return dirent_set_by_path(path, &dirent);
}

/* truncate - truncate file to exactly 'len' bytes
 * Errors - path resolution, ENOENT, EISDIR, EINVAL
 *    return EINVAL if len > 0.
 */
static int hw3_truncate(const char *path, off_t len)
{
    /* you can cheat by only implementing this for the case of len==0,
     * and an error otherwise.
     */
    if (len != 0)
	return -EINVAL;		/* invalid argument */

    return dirent_modify_by_path(path, 1, 0, -1, 0);
}

/* read - read data from an open file.
 * should return exactly the number of bytes requested, except:
 *   - if offset >= len, return 0
 *   - on error, return <0
 * Errors - path resolution, ENOENT, EISDIR
 */
static int hw3_read(const char *path, char *buf, size_t len, off_t offset,
		    struct fuse_file_info *fi)
{
    struct cs5600fs_dirent dirent;

    //get dirent of file
    int result = dirent_get_by_path(path, &dirent);
    if (result < 0)
        return result;

    //make sure offset is in range
    if (offset >= dirent.length || len == 0)
        return 0;

    //error out if directory
    if (dirent.isDir)
        return -EISDIR;

    //compute which block in the linked list to read
    int firstBlockToRead = offset / 1024;

    //compute how many bytes to read/can be read
    int bytesToRead = min(len, dirent.length - offset);

    //compute number of bytes in the first block to read
    int firstBlockOffset = (offset % 1024);
    int bytesToReadInFirstBlock = min(len, (1024 - firstBlockOffset));

    //compute number of bytes in the final block to read
    int bytesToReadInFinalBlock = (bytesToRead > bytesToReadInFirstBlock) ? (bytesToRead - bytesToReadInFirstBlock) % 1024 : 0;

    //compute how many middle blocks to read (can be read)
    int middleBlocksToRead = (bytesToRead - bytesToReadInFirstBlock - bytesToReadInFinalBlock) / 1024;

    //set up for reading data
    char tmpBlock[1024];
    char *dest = buf;
    int i = 0;
    struct cs5600fs_entry currentBlock = fat[dirent.start];
    int currentBlockIndex = dirent.start;
    assert(currentBlock.inUse);

    //move currentBlock to first block to read
    for (i = 0; i < firstBlockToRead; ++i)
    {
        currentBlockIndex = currentBlock.next;
        currentBlock = fat[currentBlock.next];
        
        assert(currentBlock.inUse);
    }

    //read first block
    if ((result = blkdev_read(currentBlockIndex, 1, (char *)&tmpBlock)) != 0)
		return NEGATIVE_RESULT(result);
		
    memcpy(dest, &tmpBlock[firstBlockOffset], bytesToReadInFirstBlock);
    dest += bytesToReadInFirstBlock;
    currentBlockIndex = currentBlock.next;
    currentBlock = fat[currentBlock.next];

    //read middle blocks
    for (i = 0; i < middleBlocksToRead ; ++i)
    {
        if ((result = blkdev_read(currentBlockIndex, 1, dest)) != 0)
			return NEGATIVE_RESULT(result);

        dest += 1024;
        currentBlockIndex = currentBlock.next;
        currentBlock = fat[currentBlock.next];

        assert(currentBlock.inUse);
    }

    //read last block
    if (bytesToReadInFinalBlock > 0)
    {
        if ((result = blkdev_read(currentBlockIndex, 1, (char *)&tmpBlock)) != 0)
			return NEGATIVE_RESULT(result);
		
        memcpy(dest, &tmpBlock, bytesToReadInFinalBlock);
    }

    return bytesToRead;
}

/* write - write data to a file
 * It should return exactly the number of bytes requested, except on
 * error.
 * Errors - path resolution, ENOENT, EISDIR
 *  return EINVAL if 'offset' is greater than current file length.
 */
static int hw3_write(const char *path, const char *buf, size_t len,
		     off_t offset, struct fuse_file_info *fi)
{
    struct cs5600fs_dirent dirent;

    //get dirent of file
    int result = dirent_get_by_path(path, &dirent);
    if (result < 0)
        return result;

    //error out if directory
    if (dirent.isDir)
        return -EISDIR;
    
    //make sure offset is in range
    if (offset > dirent.length)
        return -EINVAL;

    if (len == 0)
        return 0;

    //determine if we need to allocate blocks 
    int resultingSizeInBytes = max(offset + len, dirent.length);
    int fileBlockCount = (dirent.length > 0) ? INT_DIV_CEILING(dirent.length, 1024) : 0;
    int resultingFileBlockCount = INT_DIV_CEILING(resultingSizeInBytes, 1024); 
    int blockCountToAllocate = resultingFileBlockCount - fileBlockCount;
    int firstBlockToWrite = offset / 1024;

    int firstBlockOffset = (offset % 1024);
    int bytesToWriteInFirstBlock = min(len, 1024 - firstBlockOffset);
    int bytesToWriteInFinalBlock = (len > bytesToWriteInFirstBlock) ? (len - bytesToWriteInFirstBlock) % 1024 : 0;
    int middleBlocksToWrite = (len - bytesToWriteInFirstBlock - bytesToWriteInFinalBlock) / 1024;

    assert(((len - bytesToWriteInFirstBlock - bytesToWriteInFinalBlock) % 1024) == 0);

    //allocate new blocks 
    if (blockCountToAllocate > 0)
    {
        if (dirent.length == 0)
        {
            int newBlock = fat_allocate_blocks(blockCountToAllocate);
            dirent.start = newBlock;
        }
        else
        {
            fat_extend_blocks(&fat[dirent.start], blockCountToAllocate);
        }
    }

    //update dirent
    dirent.length = resultingSizeInBytes;
    dirent.mtime = time(NULL);
    dirent_set_by_path(path, &dirent);

    //set up
    char tmpBlock[1024];
    const char *src = buf;
    int i = 0;
    struct cs5600fs_entry currentBlock = fat[dirent.start];
    int currentBlockIndex = dirent.start;
    assert(currentBlock.inUse);

    //move currentBlock to first block 
    for (i = 0; i < firstBlockToWrite; ++i)
    {
        currentBlockIndex = currentBlock.next;
        currentBlock = fat[currentBlock.next];
        
        assert(currentBlock.inUse);
    }

    //wwrite first block
    if ((result = blkdev_read(currentBlockIndex, 1, (char *)&tmpBlock)) != 0)
		return NEGATIVE_RESULT(result);
	
    memcpy(&tmpBlock[firstBlockOffset], src, bytesToWriteInFirstBlock);
    if ((result = blkdev_write(currentBlockIndex, 1, (char *)&tmpBlock)) != 0)
		return NEGATIVE_RESULT(result);
	
    src += bytesToWriteInFirstBlock;
    currentBlockIndex = currentBlock.next;
    currentBlock = fat[currentBlock.next];

    //read middle blocks
    for (i = 0; i < middleBlocksToWrite ; ++i)
    {
        if ((result = blkdev_write(currentBlockIndex, 1, (char *)src)) != 0)
			return NEGATIVE_RESULT(result);

        src += 1024;
        currentBlockIndex = currentBlock.next;
        currentBlock = fat[currentBlock.next];

        assert(currentBlock.inUse);
    }

    //read last block
    if (bytesToWriteInFinalBlock > 0)
    {
        if ((result = blkdev_read(currentBlockIndex, 1, (char *)&tmpBlock)) != 0)
			return NEGATIVE_RESULT(result);
		
        memcpy(&tmpBlock, src, bytesToWriteInFinalBlock);
        if ((result = blkdev_write(currentBlockIndex, 1, (char *)&tmpBlock)) != 0)
			return NEGATIVE_RESULT(result);
    }

   return len; 
}

/* statfs - get file system statistics
 * see 'man 2 statfs' for description of 'struct statvfs'.
 * Errors - none. Needs to work.
 */
static int hw3_statfs(const char *path, struct statvfs *st)
{
    /* needs to return the following fields (set others to zero):
     *   f_bsize = BLOCK_SIZE
     *   f_blocks = total image - (superblock + FAT)
     *   f_bfree = f_blocks - blocks used
     *   f_bavail = f_bfree
     *   f_namelen = <whatever your max namelength is>
     *
     * it's OK to calculate this dynamically on the rare occasions
     * when this function is called.
     */

    //count number of free blocks
    int freeBlocks = 0;
    int i = 0;
    for (i = 0; i < FAT_ENTRY_COUNT(superblock.fat_len); ++i)
    {
        if (fat[i].inUse == 0)
            ++freeBlocks;
    }

    //init and set stat struct
    memset(st, 0, sizeof(struct statvfs));

    st->f_bsize = 1024;
    st->f_blocks = superblock.fs_size;
    st->f_bfree = freeBlocks;
    st->f_bavail = freeBlocks;
    st->f_namemax = 43;

    return 0;
}

/* operations vector. Please don't rename it, as the skeleton code in
 * misc.c assumes it is named 'hw3_ops'.
 */
struct fuse_operations hw3_ops = {
    .init = hw3_init,
    .getattr = hw3_getattr,
    .readdir = hw3_readdir,
    .create = hw3_create,
    .mkdir = hw3_mkdir,
    .unlink = hw3_unlink,
    .rmdir = hw3_rmdir,
    .rename = hw3_rename,
    .chmod = hw3_chmod,
    .utime = hw3_utime,
    .truncate = hw3_truncate,
    .read = hw3_read,
    .write = hw3_write,
    .statfs = hw3_statfs,
};
